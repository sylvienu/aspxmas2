namespace Xmas.DAL.Models
{
    using System;
    using System.Collections.Generic;
    using Xmas.DAL.Interface;
    using Xmas.DAL.Infra;

    public partial class CadeauEvenement: IEntity<CompositeKey<int, int>>
    {
        private int _idCadeau;
        private int _idEvenement;
        private bool _preference;
    
      

        public int IdCadeau
        {
            get
            {
                return _idCadeau;
            }

            set
            {
                _idCadeau = value;
            }
        }

        public int IdEvenement
        {
            get
            {
                return _idEvenement;
            }

            set
            {
                _idEvenement = value;
            }
        }

        public bool Preference
        {
            get
            {
                return _preference;
            }

            set
            {
                _preference = value;
            }
        }

        public Cadeau Cadeau { get; set; }
        public Evenement Evenement { get; set; }


        CompositeKey<int, int> IEntity<CompositeKey<int, int>>.Id
        {
            get
            {
                throw new NotImplementedException();
            }
        }
    }
}
