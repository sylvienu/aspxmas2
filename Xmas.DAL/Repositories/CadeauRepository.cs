﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xmas.DAL.Models;

namespace Xmas.DAL.Repositories
{
    public class CadeauRepository : BaseRepository<Cadeau, int>
    {
        public CadeauRepository(string Cnstr) : base(Cnstr)
        {
            SelectAllCommand = "SELECT * FROM Cadeau;";
            SelectOneCommand = "SELECT * FROM Cadeau WHERE IdCadeau=@IdCadeau ";
            InsertCommand = @"INSERT INTO Cadeau (Nom, Description, Magasin, Prix) VALUES (@Nom, @Description, @Magasin, @Prix) OUTPUT INSERTED IdCadeau";
            UpdateCommand = @"UPDATE Cadeau SET Nom = @Nom, Description = @Description, Magasin = @Magasin, Prix = @Prix  WHERE IdCadeau = @IdCadeau";
            DeleteCommand = @"Delete from Cadeau WHERE IdCadeau = @IdCadeau";

        }

        

        public override IEnumerable<Cadeau> getAll()
        {
            return base.getAll(Map);
        }

        public override Cadeau getOne(int id)
        {
            //SelectOneCommand = $"SELECT * FROM Cadeau WHERE idCadeau={id}";
            //return base.getOne(Map);

            // travailler avec un dictionnaire permet d'éviter les injections sql
            // vs concatenation avec requete sql directement ici

            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("idCadeau", id);
            return base.getOne(Map, QueryParameters);
        }

        public override Cadeau Insert(Cadeau toInsert)
        {
            Dictionary<string, object> parameters = MapToDico(toInsert);
            int id = base.Insert(parameters);
            toInsert.IdCadeau = id;
            return toInsert;
        }

        public override bool Update(Cadeau toUpdate)
        {
            Dictionary<string, object> parameters = MapToDico(toUpdate);
            return base.Update(parameters);
        }

        public IEnumerable<Cadeau> GetCadeauxFromMembre(int idMembre)
        {
            SelectAllCommand = @"
                                SELECT Cadeau.*
                                FROM Cadeau
                                WHERE (Cadeau.IdMembre = @id);
            ";

            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("@id", idMembre);

            return base.getAll(Map, QueryParameters);
        }

        public override bool Delete(int id)
        {
            return false;
        }

        #region Mappers
        private Dictionary<string, object> MapToDico(Cadeau toInsert)
        {
            Dictionary<string, object> p = new Dictionary<string, object>();
            p["IdCadeau"] = toInsert.IdCadeau;
            p["Nom"] = toInsert.Nom;
            p["Description"] = toInsert.Description;
            p["Magasin"] = toInsert.Magasin;
            p["Prix"] = toInsert.Prix;
            return p;
        }

        private Cadeau Map(SqlDataReader arg)
        {
            return new Cadeau()
            {
                IdCadeau = (int)arg["IdCadeau"],
                Nom = arg["Nom"].ToString(),
                Description = arg["Description"].ToString(),
                Magasin = arg["Magasin"].ToString(),
                Prix = (double)arg["Prix"]
            };
        }
        
        #endregion
    }
}
