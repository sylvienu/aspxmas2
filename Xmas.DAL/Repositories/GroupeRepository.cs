﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xmas.DAL.Models;

namespace Xmas.DAL.Repositories
{
    public class GroupeRepository : BaseRepository<Groupe, int>
    {
        public GroupeRepository(string Cnstr) :base(Cnstr)
        {
            SelectAllCommand = "SELECT * FROM Groupe;";
            InsertCommand = @"INSERT INTO Groupe (Nom, Description) 
                            VALUES (@Nom, @Description) 
                            OUTPUT INSERTED IdGroupe;";
            UpdateCommand = @"UPDATE Groupe
                            SET Nom = @Nom, Description = @Description
                            WHERE IdGroupe = @IdGroupe;";
            DeleteCommand = @"DELETE * FROM Groupe
                            WHERE IdGroupe = @IdGroupe;";
        }
        

        public override IEnumerable<Groupe> getAll()
        {
            return base.getAll(Map);
        }

        public override Groupe getOne(int id)
        {
            //SelectOneCommand = $"SELECT * FROM Groupe WHERE IdGroupe={id}";
            //return base.getOne(Map);

            // travailler avec un dictionnaire permet d'éviter les injections sql
            // vs concatenation avec requete sql directement ici

            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("idGroupe", id);
            return base.getOne(Map, QueryParameters);
        }

        public override Groupe Insert(Groupe toInsert)
        {
            Dictionary<string, object> parameters = MapToDico(toInsert);
            int id = base.Insert(parameters);
            toInsert.IdGroupe = id;
            return toInsert;
        }

        public override bool Update(Groupe toUpdate)
        {
            Dictionary<string, object> parameters = MapToDico(toUpdate);
            return base.Update(parameters);
        }

        public override bool Delete(int id)
        {
            return false;
        }

        // 1 (2 = modifier get all/protected de baseRepostory)
        // créer une fonctione permettant de récupérer groupe dont un membre est admin ou pas
        // ajout d'un paramètre par défaut booléen isAdmin à false pour pouvoir ensuite demander groupe ou le membre est admin ou non
        public IEnumerable<Groupe> getGroupeFromMember(int idMembre, bool isAdmin = false)
        {
            // on récrit la requete sql pour pouvoir utiliser une méthode déjà existant qui sera un peu modifiée
            SelectAllCommand = @"
                                SELECT Groupe.*
                                FROM Membre
                                INNER JOIN MembreGroupe ON Membre.IdMembre = MembreGroupe.IdMembre
                                INNER JOIN Groupe ON MembreGroupe.IdGroupe = Groupe.IdGroupe
                                INNER JOIN Evenement ON Groupe.IdEvenement = Evenement.IdEvenement
                                WHERE (Membre.IdMembre = @id) 
            ";
            if (isAdmin)
            {
                SelectAllCommand += " AND (MembreGroupe.Admin = true)";
            }

            // dictionnaire de paramètres meme si qu'un seul
            // dico de string/object car le getAll de Base repository dont on dépend doit recevoir un tel dico
            Dictionary<string, object> QueryParams = new Dictionary<string, object>();
            QueryParams.Add("@id", idMembre);
            return base.getAll(Map, QueryParams);

            // étant donnné qu'on ne renvoie pas des entité à la vue on va réerun modèle pour la vue
            // et transfo de Groupe/entity vers groupe/vue vie 
        }

        #region Mappers
        private Dictionary<string, object> MapToDico(Groupe toInsert)
        {
            Dictionary<string, object> p = new Dictionary<string, object>();
            p["IdGroupe"] = toInsert.IdGroupe;
            p["Nom"] = toInsert.Nom;
            p["Description"] = toInsert.Description;
            return p;
        }

        private Groupe Map(SqlDataReader arg)
        {
            return new Groupe() {
                Nom = arg["Nom"].ToString(),
                Description = arg["Description"].ToString()
            };
        }

        #endregion
    }
}
