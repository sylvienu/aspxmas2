﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xmas.DAL.Models;

namespace Xmas.DAL.Repositories
{
    public class EvenementRepository : BaseRepository<Evenement, int>
    {
        public EvenementRepository(string Cnstr) : base(Cnstr)
        {
            SelectAllCommand = "SELECT * FROM Evenement;";
            InsertCommand = @"INSERT INTO Evenement (Nom, Description, DateDebut, DateFin)
                            VALUES (@Nom, @Description, @DateDebut, @DateFin)
                            OUTPUT INSERTED IdEvenement;";
            UpdateCommand = @"UPDATE Evenement
                            SET Nom = @Nom, Description = @Description, DateDebut = @DateDebut, DateFin = @ DateFin
                            WHERE IdEvenement = @IdEvenement;";
            DeleteCommand = @"DELETE FROM Evenement WHERE IdEvenement = @IdEvenement";
        }
       

        public override IEnumerable<Evenement> getAll()
        {
            return base.getAll(Map);
        }

        public override Evenement getOne(int id)
        {
            //SelectOneCommand = $"SELECT * FROM Evenement WHERE idEvenement={id}";
            //return base.getOne(Map);
            // travailler avec un dictionnaire permet d'éviter les injections sql
            // vs concatenation avec requete sql directement ici

            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("idEvenement", id);
            return base.getOne(Map, QueryParameters);
        }

        public override Evenement Insert(Evenement toInsert)
        {
            Dictionary<string, object> parameters = MapToDico(toInsert);
            int id = base.Insert(parameters);
            toInsert.IdEvenement = id;
            return toInsert;
        }

        public override bool Update(Evenement toUpdate)
        {
            Dictionary<string, object> parameters = MapToDico(toUpdate);
            return base.Update(parameters);
        }

        public override bool Delete(int id)
        {
            return false;
        }

        #region Mappers
        private Dictionary<string, object> MapToDico(Evenement toInsert)
        {
            Dictionary<string, object> p = new Dictionary<string, object>();
            p["IdEvenement"] = toInsert.IdEvenement;
            p["Nom"] = toInsert.Nom;
            p["Description"] = toInsert.Description;
            p["DateDebut"] =toInsert.DateDebut;
            p["DateFin"] = toInsert.DateFin;

            return p;
        }

        private Evenement Map(SqlDataReader arg)
        {
            return new Evenement()
            {
                Nom = arg["Nom"].ToString(),
                Description = arg["Description"].ToString(),
                DateDebut = (DateTime)arg["DateDebut"],
                DateFin = (DateTime)arg["DateFin"]
            };
        }
        #endregion
    }
}
