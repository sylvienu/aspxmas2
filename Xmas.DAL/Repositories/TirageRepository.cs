﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xmas.DAL.Models;

namespace Xmas.DAL.Repositories
{
    public class TirageRepository : BaseRepository<Tirage, int>
    {
        public TirageRepository(string Cnstr) : base(Cnstr)
        {
            SelectAllCommand = "SELECT * FROM Tirage;";
            InsertCommand = @"INSERT INTO Tirage ()
                            VALUES ()
                            OUTPUT INSERTED IdTirage;";
            UpdateCommand = @"UPDATE Tirage
                            SET 
                            WHERE IdTirage = @IdTirage;";
            DeleteCommand = @"DELETE * FROM Tirage
                            WHERE IdTirage = @IdTirage;";
        }
        public override bool Delete(int id)
        {
            throw new NotImplementedException();
        }

        public override IEnumerable<Tirage> getAll()
        {
            throw new NotImplementedException();
        }

        public override Tirage getOne(int id)
        {
            throw new NotImplementedException();
        }

        public override Tirage Insert(Tirage toInsert)
        {
            throw new NotImplementedException();
        }

        public override bool Update(Tirage toUpdate)
        {
            throw new NotImplementedException();
        }
    }
}
