﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xmas.DAL.Models;

namespace Xmas.DAL.Repositories
{
    public class MembreRepository : BaseRepository<Membre, int>
    {
        public MembreRepository(string Cnstr) : base(Cnstr)
        {
            SelectOneCommand = @"SELECT * FROM Membre 
                               WHERE IdMembre=@idMembre;";
            SelectAllCommand = "SELECT * FROM Membre;";
            InsertCommand = @"INSERT INTO  Membre (Nom ,Prenom ,Surnom ,Courriel ,MotDePasse) 
                            OUTPUT INSERTED.IdMembre 
                            VALUES(@Nom, @Prenom, @Surnom, @Courriel, @MotDePasse);";
            UpdateCommand = @"UPDATE  Membre 
                            SET Nom = @Nom,  Prenom = @Prenom,  Surnom = @Surnom, Courriel = @Courriel,  MotDePasse = @MotDePasse 
                            WHERE IdMembre = @IdMembre;";
            DeleteCommand = @"DELETE FROM Membre 
                            WHERE IdMembre = @IdMembre;";
        }

        

        public override IEnumerable<Membre> getAll()
        {
            return base.getAll(Map);
        }

        public override Membre getOne(int id)
        {
            // travailler avec un dictionnaire permet d'éviter les injections sql
            // vs concatenation avec requete sql directement ici

            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("idMembre", id);
            return base.getOne(Map, QueryParameters);
        }

        public override Membre Insert(Membre toInsert)
        {
            Dictionary<string, object> Parameters = MapToDico(toInsert);
            int id = base.Insert(Parameters);
            toInsert.IdMembre = id;
            return toInsert;
        }

        public override bool Update(Membre toUpdate)
        {
            Dictionary<string, object> Parameters = MapToDico(toUpdate);
            return base.Update(Parameters);
             
        }

        public override bool Delete(int id)
        {
            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("@IdMembre", id);
            return base.Delete(QueryParameters);

        }

        public Membre FindMatch(string courriel, string motPasse)
        {
            SelectOneCommand = @"SELECT * FROM Membre 
                               WHERE Courriel=@Courriel AND MotDePasse=@MotDePasse;";
            // travailler avec un dictionnaire permet d'éviter les injections sql
            // vs concatenation avec requete sql directement ici

            Dictionary<string, object> QueryParameters = new Dictionary<string, object>();
            QueryParameters.Add("Courriel", courriel);
            QueryParameters.Add("MotDePasse", motPasse);
            return base.getOne(Map, QueryParameters);
        }

        #region Mappers
        private Dictionary<string, object> MapToDico(Membre toInsert)
        {
            Dictionary<string, object> p = new Dictionary<string, object>();
            // Id ne sera pas utilisé lors de l'Insert, mais sera utile lors de l'Update
            p["IdMembre"] = toInsert.Id;
            p["Nom"] = toInsert.Nom;
            p["Prenom"] = toInsert.Prenom;
            p["Surnom"] = toInsert.Surnom;
            p["Courriel"] = toInsert.Courriel;
            p["MotDePasse"] = toInsert.MotDePasse;
            return p;
        }

        private Membre Map(SqlDataReader arg)
        {
            return new Membre()
            {
                IdMembre = (int)arg["idMembre"],
                Nom = arg["Nom"].ToString(),
                Prenom = arg["Prenom"].ToString(),
                Surnom = arg["Surnom"].ToString(),
                Courriel = arg["Courriel"].ToString(),
                MotDePasse = arg["MotDePasse"].ToString()
            };

            //Membre m = new Membre();
            //m.Nom = arg["Nom"].ToString();
            //m.Prenom = arg["Prenom"].ToString();
            //m.Surnom = arg["Surnom"].ToString();
            //m.Courriel = arg["Courriel"].ToString();
            //m.MotDePasse = arg["MotDePasse"].ToString();
            //return m; 
        }
        #endregion
    }
}
