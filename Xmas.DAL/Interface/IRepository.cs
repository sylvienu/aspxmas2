﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Xmas.DAL.Interface
{
    public interface IRepository <T, TKey>
        where T: IEntity<TKey>, new()
    {
        IEnumerable<T> getAll();
        T getOne(TKey id);
        T Insert(T toInsert);
        bool Update(T toUpdate);
        bool Delete(TKey id);

    }
}
