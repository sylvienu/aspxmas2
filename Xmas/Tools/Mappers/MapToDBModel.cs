﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Xmas.Areas.Membre.Models;
using Xmas.DAL.Models;

namespace Xmas.Tools.Mappers
{
    public static class MapToDBModel
    {
        // 5 (6 -)
        public static GroupModel GroupToGroupModel(Groupe g)
        {
            return new GroupModel()
            {
                IdGroupe = g.IdGroupe,
                Description = g.Description,
                Nom = g.Nom
            };
        }

        public static CadeauModel CadeauToCadeauModel(Cadeau c)
        {
            return new CadeauModel() {
                IdCadeau = c.IdCadeau,
                Nom = c.Nom,
                Magasin = c.Magasin,
                Description = c.Description,
                Prix = c.Prix
            };
        }

        public static Cadeau CadeauModelToCadeau(CadeauModel cm)
        {
            return new Cadeau()
            {
                IdCadeau = cm.IdCadeau,
                Nom = cm.Nom,
                Magasin = cm.Magasin,
                Description = cm.Description,
                Prix = cm.Prix
            };
        }
    }
}