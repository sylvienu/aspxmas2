﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Xmas
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional },
                // cela permet d'éviter erreur lsq il y a différents controllers ayant le meme nom
                // si il y a un doute on lui donne le nom du namespace ou chercher, autrement considère tout comme gros fichier
                namespaces: new string[]{ "Xmas.Controllers" }
            );
        }
    }
}
