﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xmas.DAL.Models;
using Xmas.DAL.Repositories;
using Xmas.Models;
using Xmas.Tools;

namespace Xmas.Controllers
{
    public class HomeController : Controller
    {
        private MembreRepository Mb = new MembreRepository(@"Data Source=WAD-6\ADMINSQL;Initial Catalog=XmasDb;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        //[HttpPost]
        //public ActionResult Index(string wtf)
        //{
        //    SessionUtils.ConnectedUser = new MembreModel();
        //    return View();
        //}

        // SE LOGGER
        // lsq on clic sur MonCompte sans avoir tenté de se connecter
        [HttpGet]
        public ActionResult Login()
        {
            if (SessionUtils.ConnectedUser.Email != null)
            {
                return RedirectToAction("Index", new { controller = "Home", area = "Membre" });
            }

            return View();
        }

        // lsq on clic sur MonCompte en ayant tenté de se connecter
        [HttpPost]
        public ActionResult Login(string email, string password)
        {
            Membre Found = Mb.FindMatch(email, password);

            if (Found != null)
            {
                SessionUtils.ConnectedUser.Surnom = Found.Surnom;
                SessionUtils.ConnectedUser.Email = Found.Courriel;
                SessionUtils.ConnectedUser.Id = Found.Id;

                // si on a réussi a se connecter on met le isConnected à true
                SessionUtils.IsConnected = true;

                return RedirectToAction("Index", new { controller = "Home", area = "Membre" });
            }

            return RedirectToAction("Login", new { controller = "Home", area = "" });

        }

        // S ENREGISTRER
        [HttpPost]
        public ActionResult Register(string Nom, string Prenom, string Surnom, string Courriel, string MotDePasse, HttpPostedFileBase Picture)
        {
            Membre ToInsert = new Membre()
            {
                Nom = Nom,
                Prenom = Prenom,
                Surnom = Surnom,
                Courriel = Courriel,
                MotDePasse = MotDePasse,
            };

            Membre Inserted = Mb.Insert(ToInsert);

            string pathToFiles = Server.MapPath("~/photos/");

            System.IO.File.Delete($"{pathToFiles}{Nom}.jpg");
            Picture.SaveAs($"{pathToFiles}{Nom}.jpg");

            return RedirectToAction("Login", new { controller = "Home", area = "" });
        }
    }
}