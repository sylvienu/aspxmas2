﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xmas.Areas.Membre.Models;
using Xmas.DAL.Repositories;
using Xmas.Tools;
using Xmas.Tools.Mappers;

namespace Xmas.Areas.Membre.Controllers
{
    public class GroupController : Controller
    {
        // 3 (4 = créer modelView --> dans dossier model GroupModel pour pouvoir faire mapping)
        // avoir tous les groupes auxquel le membre appartient
        // préparer l'action qui utilisera la méthode crée dans le repo de groupe
        public ActionResult Index()
        {
            // si la personne n'ets pas connecter, la rediriger vers la page de login
            // session s'arrete apres 20min si serveur n'a plus de nouvelles
            if (!SessionUtils.IsConnected)
            {
                return RedirectToAction("Login", new { controller = "Home", area = "" });
            }

            // sinon le reste, ne s'excecute pas si pas connecté car return

            // créer un grouperepository à utiliser
            GroupeRepository GR = new GroupeRepository(ConfigurationManager.ConnectionStrings["CnstrDev"].ConnectionString);
            // définir l'id qui sera utilisé avec la fonction pour avoir les groupes, dépendant du membre connecté
            int id = SessionUtils.ConnectedUser.Id;
            // récupérer tous les groupe avec un modèle côté vue
            List<GroupModel> Lgm = GR.getGroupeFromMember(id).Select(groupeCourant => MapToDBModel.GroupToGroupModel(groupeCourant)).ToList();
            ViewBag.Current = "Groupe";

            // retourner une vue avec la liste des groupes ayant été recherchés
            return View(Lgm);
        }
    }
}