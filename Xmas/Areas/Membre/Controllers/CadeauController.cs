﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Xmas.Areas.Membre.Models;
using Xmas.DAL.Models;
using Xmas.DAL.Repositories;
using Xmas.Tools;
using Xmas.Tools.Mappers;

namespace Xmas.Areas.Membre.Controllers
{
    public class CadeauController : Controller
    {
        // GET: Membre/Cadeau
        public ActionResult Index()
        {
            // vérifier si qqn est connecté
            if (!SessionUtils.IsConnected)
            {
                return RedirectToAction("Login", new { controller = "Home", area = "" });
            }

            // faire appel au repo en lui indiquant à quelle db se connecter
            CadeauRepository CR = new CadeauRepository(ConfigurationManager.ConnectionStrings["CnstrDev"].ConnectionString);

            // récupérer l'id de l'utilisateur connecté
            int id = SessionUtils.ConnectedUser.Id;

            // créer un liste de CadeauModel
            // en récupérant l'IEnumerable que renvoie la méthode du repo
            // puis pour chaque Cadeau qu'elle contient: le transformer en CadeauModel
            // et slmnt transformer le tout en Liste
            List<CadeauModel> Lcm = CR.GetCadeauxFromMembre(id).Select(cadeauCourantDeCollectionIEnum => MapToDBModel.CadeauToCadeauModel(cadeauCourantDeCollectionIEnum)).ToList();
            return View(Lcm);
        }

        public ActionResult Edit(int id)
        {
            CadeauRepository CR = new CadeauRepository(ConfigurationManager.ConnectionStrings["CnstrDev"].ConnectionString);
            ;
            CadeauModel cm = MapToDBModel.CadeauToCadeauModel(CR.getOne(id));

            return View(cm);
        }

        [HttpPost]
        public ActionResult Edit(CadeauModel cm)
        {
            Cadeau cToUpdate = MapToDBModel.CadeauModelToCadeau(cm);
            CadeauRepository CR = new CadeauRepository(ConfigurationManager.ConnectionStrings["CnstrDev"].ConnectionString);
            if (CR.Update(cToUpdate))
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View(); //++ message d'erreur au besoin
            }
        }
    }
}